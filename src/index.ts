class ParsedBindingPeriod {
  original: string;
  months: number;
  unbound: boolean;
}

/**
 * Parses a string which describes a binding period.
 * @param period A period string, such as `12 months`, `2 years`, or `unbound`.
 */
export function parseBindingPeriod(period: string): ParsedBindingPeriod {
  if (!period) {
    return null;
  }

  if (["unbound"].indexOf(period.toLowerCase()) !== -1) {
    return {
      months: null,
      original: period,
      unbound: true
    };
  }

  const fields = period.split(" ");

  if (fields.length > 1) {
    if (/\d+\s/.test(period)) {
      if (/month/i.test(fields[1])) {
        return {
          months: Number(fields[0].trim()),
          original: period,
          unbound: false
        };
      } else if (/year/i.test(fields[1])) {
        return {
          months: Number(fields[0].trim()) * 12,
          original: period,
          unbound: false
        };
      }
    }
  }

  return null;
}

/**
 * @returns Whether its possible to change one's binding period from one to another.
 */
export function canChangeBindingPeriodTo(fromPeriod: string, toPeriod: string) {
  const fromParsed = parseBindingPeriod(fromPeriod);
  const toParsed = parseBindingPeriod(toPeriod);

  if (!fromParsed || !toParsed) {
    return false;
  }

  if (fromParsed.unbound) {
    return fromParsed.unbound !== toParsed.unbound;
  }

  if (fromParsed.months < toParsed.months) {
    return true;
  }

  return false;
}