"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ParsedBindingPeriod = /** @class */ (function () {
    function ParsedBindingPeriod() {
    }
    return ParsedBindingPeriod;
}());
/**
 * Parses a string which describes a binding period.
 * @param period A period string, such as `12 months`, `2 years`, or `unbound`.
 */
function parseBindingPeriod(period) {
    if (!period) {
        return null;
    }
    if (["unbound"].indexOf(period.toLowerCase()) !== -1) {
        return {
            months: null,
            original: period,
            unbound: true
        };
    }
    var fields = period.split(" ");
    if (fields.length > 1) {
        if (/\d+\s/.test(period)) {
            if (/month/i.test(fields[1])) {
                return {
                    months: Number(fields[0].trim()),
                    original: period,
                    unbound: false
                };
            }
            else if (/year/i.test(fields[1])) {
                return {
                    months: Number(fields[0].trim()) * 12,
                    original: period,
                    unbound: false
                };
            }
        }
    }
    return null;
}
exports.parseBindingPeriod = parseBindingPeriod;
/**
 * @returns Whether its possible to change one's binding period from one to another.
 */
function canChangeBindingPeriodTo(fromPeriod, toPeriod) {
    var fromParsed = parseBindingPeriod(fromPeriod);
    var toParsed = parseBindingPeriod(toPeriod);
    if (!fromParsed || !toParsed) {
        return false;
    }
    if (fromParsed.unbound) {
        return fromParsed.unbound !== toParsed.unbound;
    }
    if (fromParsed.months < toParsed.months) {
        return true;
    }
    return false;
}
exports.canChangeBindingPeriodTo = canChangeBindingPeriodTo;
