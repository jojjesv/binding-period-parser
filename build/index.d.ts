declare class ParsedBindingPeriod {
    original: string;
    months: number;
    unbound: boolean;
}
/**
 * Parses a string which describes a binding period.
 * @param period A period string, such as `12 months`, `2 years`, or `unbound`.
 */
export declare function parseBindingPeriod(period: string): ParsedBindingPeriod;
/**
 * @returns Whether its possible to change one's binding period from one to another.
 */
export declare function canChangeBindingPeriodTo(fromPeriod: string, toPeriod: string): boolean;
export {};
